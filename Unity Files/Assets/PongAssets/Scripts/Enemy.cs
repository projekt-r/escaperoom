﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public float speed = 1.75F;
	Transform ball;
	Rigidbody ballRig;

	public float topBound = 4.5F;
	public float bottomBound = -4.5F;

      void Start () {
        InvokeRepeating("Move", .02F, .02F);
    }

    void Move () {

        if(ball == null){
            ball = GameObject.FindGameObjectWithTag("Ball").transform;
        }

        ballRig = ball.GetComponent<Rigidbody>();

      
        if(ball.position.y < this.transform.position.y-.5F){
            transform.Translate(Vector3.down*speed*Time.deltaTime);
        } else if(ball.position.y > this.transform.position.y+.5F){
          transform.Translate(Vector3.up*speed*Time.deltaTime);
        }

       if(transform.position.y > topBound){
            transform.position = new Vector3(transform.position.x, topBound, 0);
        } else if(transform.position.y < bottomBound){
            transform.position = new Vector3(transform.position.x, bottomBound, 0);
        }
    }
}
