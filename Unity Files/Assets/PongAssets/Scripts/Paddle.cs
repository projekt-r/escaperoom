using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    public bool isPlayer1;
    public float speed;
    public Rigidbody rb;
    public Vector3 startPosition;

    private float movement; 
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startPosition = transform.position;
    }
    void Update()
    {
        if (isPlayer1)
        {
            movement = Input.GetAxisRaw("Vertical");
        }

        rb.velocity = new Vector3(rb.velocity.x, movement * speed, 0.0f);
    }

    public void Reset()
    {
        rb.velocity = Vector3.zero;
        transform.position = startPosition;
    }
}
