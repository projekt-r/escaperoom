﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed;
    public Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke("Launch", 2);
        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(rb.velocity);
    }

    private void Launch()
    {
        float rand = Random.Range(0, 2);
        if(rand < 1){
            rb.AddForce(new Vector3(20, -15, 0));
        } else {
            rb.AddForce(new Vector3(-20, -15, 0));
        }

    }

    public void ResetBall(){
        rb.velocity = Vector2.zero;
        transform.position = Vector2.zero;
    }   

    void RestartGame(){
        ResetBall();
        Invoke("Launch", 1);
    }

    void OnCollisionEnter(Collision coll) {
        Debug.Log(rb.velocity);
        if(coll.collider.CompareTag("Player")){
            Vector3 vel;
            vel.x = -rb.velocity.x;
            vel.y = rb.velocity.y;
            vel.z = 0.0f;
            rb.velocity = vel;
        }
    }


}
