﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StackControl : MonoBehaviour
{
    int stack = 0;
    public List<GameObject> children;
    public int tickMax = 15;

    void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Player"))
            {
                children.Add(child.gameObject);
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyUp("space"))    
        {
            children[stack].GetComponent<Movement>().enabled = false;

            if (stack > 0 && !CorrectStack(children[stack - 1], children[stack]))
            {
                Debug.Log("game over");
                enabled = false;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            stack++;
            if (stack < children.Count)
            {
                if (enabled)
                {
                    tickMax = 19 - stack;
                    children[stack].GetComponent<Movement>().tickMax = tickMax;
                    children[stack].SetActive(true);
                }
            }
            else
            {
                Debug.Log("you win");
                enabled = false;
            }
        }
    }

    private bool CorrectStack(GameObject bot, GameObject top)
    {
        var topLeft = top.transform.localPosition.x - (top.GetComponent<Movement>().size / 2);
        var topRight = top.transform.localPosition.x + (top.GetComponent<Movement>().size / 2);
        var botLeft = bot.transform.localPosition.x - (bot.GetComponent<Movement>().size / 2);
        var botRight = bot.transform.localPosition.x + (bot.GetComponent<Movement>().size / 2);
        if (topLeft >= botLeft && topRight <= botRight)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
