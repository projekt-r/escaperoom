﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    private int direction = 1;
    private int tick = 0;
    public float size = 3;
    public int tickMax = 15;
    private float borderLeft = 0.0f;
    private float borderRight = 10.0f;

    void Start()
    {
        direction = Random.Range(0, 2) * 2 - 1;
        borderLeft = (0.5f * size) + 1.5f;
        borderRight = -(0.5f * size) + 8.5f;
    }

    private void FixedUpdate()
    {
        if (tick == tickMax)
        {
            transform.position += new Vector3(direction, 0, 0);
            CheckCollision();
            tick = 0;
        }
        else
        {
            tick++;
        }   
    }

    private void CheckCollision()
    {
        if (transform.localPosition.x == borderLeft)
        {
            direction = 1;
        }
        else if(transform.localPosition.x == borderRight)
        {
            direction = -1;
        }
    }
}
