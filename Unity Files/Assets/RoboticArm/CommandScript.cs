﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandScript : MonoBehaviour
{

    private bool pressed = false;
    void Start()
    {
        pressed = false;
    }

    private void OnMouseDown() {
        pressed = true;
        gameObject.transform.Translate(0.0f, 0.0f, -0.005f);
    }

    private void OnMouseUp() {
        pressed = false;
        gameObject.transform.Translate(0.0f, 0.0f, 0.005f);
    }

    public bool IsPressed(){
        return pressed;
    }
}
