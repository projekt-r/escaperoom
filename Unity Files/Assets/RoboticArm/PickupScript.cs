﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupScript : MonoBehaviour
{

    private Transform objectToPickup;
    private bool colliding;
    private bool pickupCondition;
    private bool lastPickupCondition;
    private bool pickedUp;
    public CommandScript pickupButton;
    
    // Start is called before the first frame update
    void Start()
    {
        objectToPickup = null;
        colliding = false;
        lastPickupCondition = false;
        pickedUp = false;
    }

    private void Update() {
        lastPickupCondition = pickupCondition;
        pickupCondition = pickupButton.IsPressed();

        if(objectToPickup != null && Input.GetKeyDown(KeyCode.Space)){
            Debug.Log(objectToPickup.gameObject.GetComponent<Rigidbody>().velocity);
            Debug.Log(objectToPickup.gameObject.GetComponent<Rigidbody>().position);
        }

        if(pickupCondition && !lastPickupCondition){
            if(gameObject.transform.childCount == 0 && colliding){
                objectToPickup.parent = gameObject.transform;
                objectToPickup.localPosition = new Vector3(0.0f, 1.8f, 0.0f);
                objectToPickup.gameObject.GetComponent<Rigidbody>().isKinematic = true;

                pickedUp = true;
            }else if(gameObject.transform.childCount > 0 && pickedUp){
                objectToPickup.parent = null;

                objectToPickup.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                objectToPickup.gameObject.GetComponent<Rigidbody>().WakeUp();
                objectToPickup.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

                objectToPickup = null;
                colliding = false;

                pickedUp = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(!pickedUp){
            objectToPickup = other.transform;
            colliding = true;
        }

        Debug.Log("Trigger entered!");
    }

    private void OnTriggerExit(Collider other) {
        if(!pickedUp){
            objectToPickup = null;
            colliding = false;
        }
        
        Debug.Log("Trigger exited!");
    }

}
