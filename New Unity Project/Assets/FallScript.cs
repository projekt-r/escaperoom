﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallScript : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.name == "RoboticArmHead") 
        {
            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            gameObject.GetComponent<Rigidbody>().useGravity = true;

        }
            
    }
}
