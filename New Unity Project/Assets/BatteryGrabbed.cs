﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class BatteryGrabbed : MonoBehaviour
{
    private XRGrabInteractable simpleInteractable;
    private GameObject player;
    void Start()
    {
        player = GameObject.Find("Player");
        simpleInteractable = GetComponent<XRGrabInteractable>();
        simpleInteractable.onSelectEntered.AddListener(BatteryAquired);
    }

    private void BatteryAquired(XRBaseInteractor interactor) {
        player.GetComponent<PlayerScript>().AddBattery();
        gameObject.SetActive(false);
    }
}
