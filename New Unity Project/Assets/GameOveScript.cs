﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOveScript : MonoBehaviour
{
    public void EndGame()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
