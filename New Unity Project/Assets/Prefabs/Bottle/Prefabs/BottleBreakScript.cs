﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleBreakScript : MonoBehaviour
{

    public float breakingThreshold = 2.0f;
    
    public Transform brokenBottle;
    public Transform battery;

    private void OnCollisionEnter(Collision other) {
        Debug.Log(gameObject.GetComponent<Rigidbody>().velocity.magnitude);
        
        if(gameObject.GetComponent<Rigidbody>().velocity.magnitude > breakingThreshold){
            Instantiate(brokenBottle, transform.localPosition, transform.rotation);
            Instantiate(battery, transform.localPosition, transform.rotation);
            Destroy(gameObject);
        }
    }
}
