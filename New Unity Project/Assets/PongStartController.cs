using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PongStartController : MonoBehaviour
{
    public XRSimpleInteractable simpleInteractable;
    private GameObject ball;
    private GameObject player;
    private GameObject enemy;
    void Start()
    {
        ball = GameObject.Find("Ball");
        player = GameObject.Find("Player1"); 
        enemy = GameObject.Find("Player2");
        simpleInteractable = GetComponent<XRSimpleInteractable>();
        simpleInteractable.onSelectExited.AddListener(OnMouseUp);
    }

    private void OnMouseUp(XRBaseInteractor interactor) {
        ball.GetComponent<Ball>().RestartGame();
        enemy.transform.localPosition = new Vector3(8, 0, 0);
        player.transform.localPosition = new Vector3(-8, 0, 0);
        gameObject.transform.Translate(0.0f, 0.0f, 0.0025f);
    }
}
