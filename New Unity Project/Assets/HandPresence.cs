﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandPresence : MonoBehaviour
{
    public InputDeviceCharacteristics controllerCharact;
    private InputDevice targetDevice;
    public List<GameObject> controllerPrefabs;
    private GameObject spawnedController;

    // Start is called before the first frame update
    void Start()
    {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(controllerCharact, devices);   

        if(devices.Count > 0)
        {
            targetDevice = devices[0];
            Debug.Log(targetDevice.name);
            GameObject prefab = controllerPrefabs.Find(controller => controller.name == targetDevice.name); 
            if(prefab)
            {
                spawnedController = Instantiate(prefab, transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
