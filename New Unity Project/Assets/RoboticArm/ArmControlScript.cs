﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmControlScript : MonoBehaviour
{

    public Transform target;
    public Transform stand;

    public CommandScript clockwiseButton;
    public CommandScript counterclockwiseButton;
    public CommandScript upButton;
    public CommandScript rightButton;
    public CommandScript downButton;
    public CommandScript leftButton;

    public float movingSpeed;
    public float rotationSpeed;

    private float theta;

    void Start(){
        theta = 0.0f;

        stand.localEulerAngles = new Vector3(stand.localEulerAngles.x, theta, stand.localEulerAngles.z);
    }
    // Update is called once per frame
    void Update()
    {
        float x = target.localPosition.x;
        float y = target.localPosition.y;

        bool upCondition = upButton.IsPressed();
        bool rightCondition = rightButton.IsPressed();
        bool downCondition = downButton.IsPressed();
        bool leftCondition = leftButton.IsPressed();
        bool clockwiseCondition = clockwiseButton.IsPressed();
        bool counterclockwiseCondition = counterclockwiseButton.IsPressed();

        if(upCondition && y*y + x*x < 0.024f*0.024f){
            // Debug.Log("Up Arrow Pressed");
            y += movingSpeed * Time.deltaTime;
        }else if(downCondition && y > 0.0f){
            // Debug.Log("Down Arrow Pressed");
            y -= movingSpeed * Time.deltaTime;
        }else if(leftCondition && x > 0.001f){
            // Debug.Log("Left Arrow Pressed");
            x -= movingSpeed * Time.deltaTime;
        }else if(rightCondition && x*x + y*y < 0.024f*0.024f){
            // Debug.Log("Right Arrow Pressed");
            x += movingSpeed * Time.deltaTime;
        }

        target.localPosition = new Vector3(x, y, 0.0f);

        if(clockwiseCondition){
            // Debug.Log("A Key Pressed");
            theta -= rotationSpeed * Time.deltaTime;
            stand.localEulerAngles = new Vector3(stand.localEulerAngles.x, theta, stand.localEulerAngles.z);
        }else if(counterclockwiseCondition){
            // Debug.Log("D Key Pressed");
            theta += rotationSpeed * Time.deltaTime;
            stand.localEulerAngles = new Vector3(stand.localEulerAngles.x, theta, stand.localEulerAngles.z);
        }
    }

     void OnCollisionEnter(Collision collision)
    {
        Debug.Log("hit");
        //Check for a match with the specified name on any GameObject that collides with your GameObject
        if (collision.gameObject.name == "MyGameObjectName")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            Debug.Log("Do something here");
        }

        //Check for a match with the specific tag on any GameObject that collides with your GameObject
        if (collision.gameObject.tag == "MyGameObjectTag")
        {
            //If the GameObject has the same tag as specified, output this message in the console
            Debug.Log("Do something else here");
        }
    }

}
