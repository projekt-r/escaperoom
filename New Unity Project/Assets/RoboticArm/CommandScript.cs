﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CommandScript : MonoBehaviour
{

    public XRSimpleInteractable simpleInteractable;

    private bool pressed = false;
    void Start()
    {
        pressed = false;
        simpleInteractable = GetComponent<XRSimpleInteractable>();
        simpleInteractable.onSelectEntered.AddListener(OnMouseDown);
        simpleInteractable.onSelectExited.AddListener(OnMouseUp);
    }

    private void OnMouseDown(XRBaseInteractor interactor) {
        pressed = true;
        gameObject.transform.Translate(0.0f, 0.0f, -0.0025f);
    }

    private void OnMouseUp(XRBaseInteractor interactor) {
        pressed = false;
        gameObject.transform.Translate(0.0f, 0.0f, 0.0025f);
    }

    public bool IsPressed(){
        return pressed && GameObject.Find("Player").GetComponent<PlayerScript>().GetBatteries() >= 2;
    }

    public void testFunction(){
        Debug.Log("bokic ja radimmm!!!");
    }

    public void SetEnabled() {
        enabled = true;
    }
}
