﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    private Button Play;

    [SerializeField]
    private Button About;

    [SerializeField]
    private Button Quit;

    void Start()
    {
        Play.onClick.AddListener(() => {
            SceneManager.LoadScene("MainScene");
        });

        About.onClick.AddListener(() => {
            Debug.Log("about clicked");
        });

        Quit.onClick.AddListener(() => {
            Application.Quit();
        });
    }

}
