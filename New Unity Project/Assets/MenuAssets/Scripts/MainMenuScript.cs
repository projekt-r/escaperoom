﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public void playGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void quitGame()
    {
        Debug.Log("Quit game clicked");
        Application.Quit();
    }

}
