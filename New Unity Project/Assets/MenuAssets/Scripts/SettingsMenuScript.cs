﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingsMenuScript : MonoBehaviour
{

    public Button applyButton;

    public TMP_Dropdown dropdown;

    private void OnEnable()
    {
        int numberOfGames = PlayerPrefs.GetInt("NUMBER_OF_GAMES", 4);
        dropdown.value = dropdown.options.FindIndex(option => option.text == numberOfGames.ToString());
    }

    public void setRayTracing(bool value)
    {
        PlayerPrefs.SetInt("RayTracing", value ? 1 : 0);
    }

    public void applySettings()
    {
        PlayerPrefs.SetInt("NUMBER_OF_GAMES", int.Parse(dropdown.options[dropdown.value].text));
    }

}
