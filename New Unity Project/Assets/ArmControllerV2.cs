﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ArmControllerV2 : MonoBehaviour
{

    public XRSimpleInteractable simpleInteractable;
    void Start()
    {
        simpleInteractable = GetComponent<XRSimpleInteractable>();
        simpleInteractable.onHoverEntered.AddListener(Test);
        simpleInteractable.onSelectEntered.AddListener(Test2);
    }

    void OnDestroy() 
    {
        simpleInteractable.onHoverEntered.RemoveListener(Test);
        simpleInteractable.onSelectEntered.RemoveListener(Test2);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Test(XRBaseInteractor interactor)
    {
        Debug.Log("test");
    }

    private void Test2(XRBaseInteractor interactor)
    {
        Debug.Log("test2");
    }

}
