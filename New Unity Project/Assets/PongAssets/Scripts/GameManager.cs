﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Ball")]
    public GameObject ball;
    
    [Header("Player 1")]
    public GameObject player1Paddle;
    public GameObject player1Goal;

    [Header("Player 2")]
    public GameObject player2Paddle;
    public GameObject player2Goal;    

    public static int Player1Score = 0;
    public static int Player2Score = 0;

    void Start () {
        ball = GameObject.FindGameObjectWithTag("Ball");
    }

    public static void Score (string wallID) {
        if (wallID == "Player1Goal")
        {
            Player1Score++;
        } else
        {
            Player2Score++;
        }

        Debug.Log(Player1Score + " : " + Player2Score);
    }

    private void ResetPosition(){
        ball.GetComponent<Ball>().ResetBall();
        player1Paddle.GetComponent<Paddle>().Reset();
        player2Paddle.GetComponent<Paddle>().Reset();
    }
   
}
