﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
	Transform ball;
	Rigidbody ballRig;

    public float topBound;
    public float bottomBound;

    void Start () {
        InvokeRepeating("Move", .02F, .02F);
    }

    void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0.0f);  
    }
    void Move () {

        if(ball == null){
            ball = GameObject.FindGameObjectWithTag("Ball").transform;
        }

        ballRig = ball.GetComponent<Rigidbody>();

      
        if(ball.localPosition.y < this.transform.localPosition.y-.5F){
            transform.Translate(Vector3.down*speed*Time.deltaTime);
        } else if(ball.localPosition.y > this.transform.localPosition.y+.5F){
          transform.Translate(Vector3.up*speed*Time.deltaTime);
        }

        if (transform.localPosition.y > topBound)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, topBound, 0);
        }
        else if (transform.position.y < bottomBound)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, bottomBound, 0);
        }
    }
}
