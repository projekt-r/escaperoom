using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Paddle : MonoBehaviour
{
    public bool isPlayer1;
    public float speed;
    public Rigidbody rb;
    public Vector3 startPosition;
    public int direction;

    private float movement; 

    public float topBound;
    public float bottomBound;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startPosition = transform.position;
    }
    void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0.0f);
        rb.velocity = new Vector3(0.0f, direction * speed, 0.0f);

        if (transform.localPosition.y > topBound)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, topBound, 0);
        }
        else if (transform.position.y < bottomBound)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, bottomBound, 0);
        }
    }

    public void Reset()
    {
        rb.velocity = Vector3.zero;
        transform.position = startPosition;
    }

    public void OnSelectEntered(XRSimpleInteractable interaction) 
    {

    }
}
