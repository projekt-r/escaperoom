﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float speed;
    public Rigidbody rb;
    private float minVelocity = 1f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
    }
    void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0.0f);
    }

    private void Launch()
    {
        float rand = Random.Range(0, 2);
        if(rand < 1){
            rb.AddForce(transform.TransformDirection(new Vector3(3f, -1f, 0)));
        } else {
            rb.AddForce(transform.TransformDirection(new Vector3(-3f, -1f, 0)));
        }

    }

    public void ResetBall(){
        rb.velocity = Vector3.zero;
        transform.localPosition = Vector3.zero;
    }   

    public void RestartGame(){
        ResetBall();
        Invoke("Launch", 1);
    }

    void OnCollisionEnter(Collision coll) {
        if(coll.collider.CompareTag("Player")){
            var locVel = transform.InverseTransformDirection(rb.velocity);
            locVel.x = -locVel.x;
            locVel.y = locVel.y;
            locVel.z = 0.0f;
            rb.velocity = transform.TransformDirection(locVel);
        }
        if(coll.collider.name == "BottomWall" || coll.collider.name == "TopWall")
        {
            var speed = rb.velocity.magnitude;
            var direction = Vector3.Reflect(rb.velocity.normalized, coll.contacts[0].normal);
            direction.z = 0.0f;
            rb.velocity = direction * Mathf.Max(speed, minVelocity);
        }
    }

}
