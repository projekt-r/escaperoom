﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerScript : MonoBehaviour
{
    private int batteries = 0;

    public TextMeshProUGUI batteryUI;

    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddBattery()
    {
        batteries++;
        batteryUI.text = "Batteries: " + batteries.ToString();
    }

    public int GetBatteries()
    {
        return batteries;
    }

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("aaaa");
        Debug.Log(col.collider.tag);
    }
}
