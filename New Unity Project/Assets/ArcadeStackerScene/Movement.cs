﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    public int direction {get; set;}
    private int tick = 0;
    public float size = 3;
    public int tickMax = 15;
    private float borderLeft = -3.5f;
    private float borderRight = 3.5f;

    void Start()
    {
        direction = Random.Range(0, 2) * 2 - 1;
        borderLeft = (0.5f * size) + 1.5f;
        borderRight = -(0.5f * size) + 8.5f;
    }

    private void FixedUpdate()
    {
        if (tick == tickMax)
        {
            transform.localPosition += new Vector3(direction, 0, 0);
            CheckCollision();
            tick = 0;
        }
        else
        {
            tick++;
        }   
    }

    private void CheckCollision()
    {
        if (transform.localPosition.x <= borderLeft)
        {
            direction = 1;
        }
        else if(transform.localPosition.x >= borderRight)
        {
            direction = -1;
        }
    }
}
