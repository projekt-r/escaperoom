﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Interaction.Toolkit;
using System;


public class StackControl : MonoBehaviour
{
    int stack = 0;
    public List<GameObject> children;
    public int tickMax = 15;
    public XRSimpleInteractable simpleInteractable;
    private bool buttonPressed = false;
    public GameObject stackerBattery;


    void Start()    
    {
        simpleInteractable.onSelectEntered.AddListener(TriggerPressed);
        Setup();
    }

    void Update()
    {
        if (buttonPressed && enabled)
        {
            children[stack].GetComponent<Movement>().enabled = false;

            if (stack > 0 && !CorrectStack(children[stack - 1], children[stack]))
            {
                Debug.Log("game over");
                enabled = false;
                Setup();
                buttonPressed = false;
                return;
                //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

            stack++;
            if (stack < children.Count)
            {
                if (enabled)
                {
                    tickMax = 19 - stack;
                    children[stack].GetComponent<Movement>().tickMax = tickMax;
                    children[stack].SetActive(true);
                }
            }
            else
            {
                stackerBattery.SetActive(true);
                enabled = false;
            }
            buttonPressed = false;
        }
    }

    private bool CorrectStack(GameObject bot, GameObject top)
    {
        var topLeft = top.transform.localPosition.x - (top.GetComponent<Movement>().size / 2);
        var topRight = top.transform.localPosition.x + (top.GetComponent<Movement>().size / 2);
        var botLeft = bot.transform.localPosition.x - (bot.GetComponent<Movement>().size / 2);
        var botRight = bot.transform.localPosition.x + (bot.GetComponent<Movement>().size / 2);
        if (Math.Floor(topLeft) >= Math.Floor(botLeft)&& Math.Floor(topRight) <= Math.Floor(botRight))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void TriggerPressed(XRBaseInteractor interactor) { buttonPressed = true; }

    void Setup() 
    {
        stack = 0;
        children = new List<GameObject>();
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Player"))
            {
                children.Add(child.gameObject);
            }
        }
        foreach (var child in children)
        {
            child.SetActive(false);
            child.GetComponent<Movement>().enabled = true;
            child.GetComponent<Movement>().direction = 1;
        }
        children[0].SetActive(true);
        //children[0].GetComponent<Movement>().enabled = true;
        enabled = true;
    }
}
