﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class PongPadleController : MonoBehaviour
{
    public XRSimpleInteractable simpleInteractable;
    private GameObject paddle;
    public int direction;
    void Start()
    {
        paddle = GameObject.Find("Player1");
        simpleInteractable = GetComponent<XRSimpleInteractable>();
        simpleInteractable.onSelectEntered.AddListener(OnMouseDown);
        simpleInteractable.onSelectExited.AddListener(OnMouseUp);
    }

    private void OnMouseDown(XRBaseInteractor interactor) {
        paddle.GetComponent<Paddle>().direction = direction;
        gameObject.transform.Translate(0.0f, 0.0f, -0.0025f);
    }

    private void OnMouseUp(XRBaseInteractor interactor) {
        paddle.GetComponent<Paddle>().direction = 0;
        gameObject.transform.Translate(0.0f, 0.0f, 0.0025f);
    }
}
